class Init():
    def __init__(self, plcIP, iocIP, plcOpcName):
        self.PlcIp = plcIP
        self.IocIp = iocIP
        self.OpcName = plcOpcName


class Analog:
    def __init__(self, tag, opcInNode, pvName, valLOLO, valLO, valHI, valHIHI, rangeLO, rangeHI, checkVal1, checkVal2, checkVal3, checkVal4):
        self.Tag = tag                  # vals[2]
        self.OpcInNode = opcInNode      # vals[3]
        self.PvName = pvName            # vals[4]
        self.ValLoLo = valLOLO          # vals[5]
        self.ValLo = valLO              # vals[6]
        self.ValHi = valHI              # vals[7]
        self.ValHiHi = valHIHI          # vals[8]
        self.RngLo = rangeLO            # vals[9]
        self.RngHi = rangeHI            # vals[10]
        self.ChkVal1 = checkVal1        # vals[11]
        self.ChkVal2 = checkVal2        # vals[12]
        self.ChkVal3 = checkVal3        # vals[13]
        self.ChkVal4 = checkVal4        # vals[14]


class CV:
    def __init__(self, tag, opcInNode, opcOutNode, opcStatPnRNode, pvName):
        self.Tag = tag        # vals[2]
        self.FeedBack = opcInNode   # vals[3]
        self.SetPoint = opcOutNode  # vals[4]
        self.StaPnR = opcStatPnRNode  # vals[5]
        self.PvName = pvName  # vals[6]


class ON_OFF_VALVE:
    def __init__(self, tag, opcInOpenNode, opcInCloseNode, opcOutNode, opcStatPnRNode, pvName):
        self.Tag = tag                        # vals[2]
        self.FeedBack_open = opcInOpenNode    # vals[3]
        self.FeedBack_close = opcInCloseNode  # vals[4]
        self.Command = opcOutNode             # vals[5]
        self.StaPnR = opcStatPnRNode          # vals[6]
        self.PvName = pvName                  # vals[7]


class PUMP:
    def __init__(self, tag, opcInNode, opcOutNode, pvName):
        self.Tag = tag        # vals[2]
        self.Status = opcInNode   # vals[3]
        self.Control = opcOutNode  # vals[4]
        self.PvName = pvName       # vals[5]


class LBCA:
    def __init__(self, tag, opcInNode, pvName):
        self.Tag = tag
        self.PlcIn = opcInNode
        self.PvName = pvName
