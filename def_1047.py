from DeviceTypes import OnOffValve, ControlValve, DiscretePump, AnalogDevice, LevelSwitch
from myLib import scale
import time

exitFlag = 0

def simulator1047(threadName, client, plcName, timeout):
    # Devices
    LIA101_1047 = AnalogDevice
    LIA111_1047 = AnalogDevice
    LIA121_1047 = AnalogDevice
    PIT115_1047 = AnalogDevice
    PIT125_1047 = AnalogDevice
    YSV200 = OnOffValve(client, plcName, '3:1047_YSV_200_PosOpen',
                        '3:1047_YSV_200_PosClose', '3:1047_YSV_200_CmdOpCo')
    YSV110 = OnOffValve(client, plcName, '3:1047_YSV_110_PosOpen',
                        '3:1047_YSV_110_PosClose', '3:1047_YSV_110_CmdOpCo')
    YSV210 = OnOffValve(client, plcName, '3:1047_YSV_210_PosOpen',
                        '3:1047_YSV_210_PosClose', '3:1047_YSV_210_CmdOpCo')
    YSV100 = OnOffValve(client, plcName, '3:1047_YSV_100_PosOpen',
                        '3:1047_YSV_100_PosClose', '3:1047_YSV_100_CmdOpCo')
    YSV220 = OnOffValve(client, plcName, '3:1047_YSV_220_PosOpen',
                        '3:1047_YSV_220_PosClose', '3:1047_YSV_220_CmdOpCo')
    YSV230 = OnOffValve(client, plcName, '3:1047_YSV_230_PosOpen',
                        '3:1047_YSV_230_PosClose', '3:1047_YSV_230_CmdOpCo')
    FCV160 = ControlValve(
        client, plcName, '3:1047_HIC_160_PosFB', '3:1047_HIC_160_CmdPos')
    FCV170 = ControlValve(
        client, plcName, '3:1047_HIC_170_PosFB', '3:1047_HIC_170_CmdPos')
    FCV240 = ControlValve(
        client, plcName, '3:1047_HIC_240_PosFB', '3:1047_HIC_240_CmdPos')
    FCV120 = ControlValve(
        client, plcName, '3:1047_HIC_120_PosFB', '3:1047_HIC_120_CmdPos')
    FCV180 = ControlValve(
        client, plcName, '3:1047_HIC_180_PosFB', '3:1047_HIC_180_CmdPos')
    P012 = DiscretePump(
        client, plcName, '3:1047_P_012_CmdRun', '3:1047_P_012_StaRun')
    P013 = DiscretePump(
        client, plcName, '3:1047_P_013_CmdRun', '3:1047_P_013_StaRun')
    LS122 = LevelSwitch

    # Initializaton
    level_101 = 50.00000
    level_111 = 50.00000
    level_121 = 50.00000
    press_115 = 0.00000
    press_125 = 0.00000

    YSV200_status = "Close"
    YSV110_status = "Close"
    YSV210_status = "Close"
    YSV100_status = "Close"
    YSV220_status = "Close"
    YSV230_status = "Close"
    FCV160_status = 0
    FCV170_status = 0
    FCV240_status = 0
    FCV120_status = 0
    FCV180_status = 0

    P012_status = False
    P013_status = False

    # SIMulation main cycle
    while (time.time() < timeout):
        # Main
        if exitFlag:
            threadName.exit()
    # Draining
        if YSV200_status == "Open":
            level_101 += 1

        if YSV210_status == "Open":
            level_111 += 1

        if (YSV220_status == "Open") and (YSV230_status == "Open"):
            level_121 += 1
        if level_121 >= 96:
            LS122(client, plcName, '3:1047_LS_122_SWActiv', True)
        else:
            LS122(client, plcName, '3:1047_LS_122_SWActiv', False)

    # pumping
        if P012_status and (YSV110_status == "Open") and ((FCV160_status == scale(80)) or (FCV180_status == scale(80))):
            level_101 -= 0.0001
            press_115 = 2.1
        else:
            press_115 = 0

        if P012_status and (YSV100_status == "Open") and ((FCV170_status == scale(80)) or (FCV180_status == scale(80))):
            level_111 -= 0.0001
            press_115 = 2.2
        else:
            press_115 = 0

        if P013_status and ((FCV240_status == scale(80)) or (FCV120_status == scale(80))):
            level_121 -= 0.0001
            press_125 = 3.2
        else:
            press_125 = 0

        if level_101 < 0:
            level_101 = 0
        if level_111 < 0:
            level_111 = 0
        if level_121 < 0:
            level_121 = 0

        if level_101 > 100:
            level_101 = 100
        if level_111 > 100:
            level_111 = 100
        if level_121 > 100:
            level_121 = 100

        # Values to devices:
        LIA101_1047(client, plcName, '3:1047_PIT_101_TReading',
                    scale(level_101))
        LIA111_1047(client, plcName, '3:1047_PIT_111_TReading',
                    scale(level_111))
        LIA121_1047(client, plcName, '3:1047_PDIT_121_TReading',
                    scale(level_121))
        PIT115_1047(client, plcName, '3:1047_PIT_115_TReading',
                    scale(press_115))
        PIT125_1047(client, plcName, '3:1047_PIT_125_TReading',
                    scale(press_125))

        YSV200_status = YSV200.SIM()
        YSV110_status = YSV110.SIM()
        YSV210_status = YSV210.SIM()
        YSV100_status = YSV100.SIM()
        YSV220_status = YSV220.SIM()
        YSV230_status = YSV230.SIM()
        FCV160_status = FCV160.SIM()
        FCV170_status = FCV170.SIM()
        FCV240_status = FCV240.SIM()
        FCV120_status = FCV120.SIM()
        FCV180_status = FCV180.SIM()

        P012_status = P012.SIM()
        P013_status = P013.SIM()

        #print(threadName + ' Simulation Running!')
