from DeviceTypes import OnOffValve, ControlValve, DiscretePump, AnalogDevice, LevelSwitch
from opc_client import OPCClient
from myLib import scale

# Constants
ip = "192.168.0.223"
plcName = '3:DRCS_PLC001'

valveOpenVal = 80
valveCloseVal = 10

# Connect to PLC
client = OPCClient(ip, timeout=600)
client.connect()

# Devices
LIA106 = AnalogDevice
LIA108 = AnalogDevice
LIA110 = AnalogDevice
PIT001 = AnalogDevice
YSV002 = OnOffValve(client, plcName, '3:1048_YSV_002_PosOpen', '3:1048_YSV_002_PosClose', '3:1048_YSV_002_CmdOpCo')
YSV005 = OnOffValve(client, plcName, '3:1048_YSV_005_PosOpen', '3:1048_YSV_005_PosClose', '3:1048_YSV_005_CmdOpCo')
YSV008 = OnOffValve(client, plcName, '3:1048_YSV_008_PosOpen', '3:1048_YSV_008_PosClose', '3:1048_YSV_008_CmdOpCo')
YSV130 = OnOffValve(client, plcName, '3:1048_YSV_130_PosOpen', '3:1048_YSV_130_PosClose', '3:1048_YSV_130_CmdOpCo')
YSV132 = OnOffValve(client, plcName, '3:1048_YSV_132_PosOpen', '3:1048_YSV_132_PosClose', '3:1048_YSV_132_CmdOpCo')
YSV134 = OnOffValve(client, plcName, '3:1048_YSV_134_PosOpen', '3:1048_YSV_134_PosClose', '3:1048_YSV_134_CmdOpCo')
YSV137 = OnOffValve(client, plcName, '3:1048_YSV_137_PosOpen', '3:1048_YSV_137_PosClose', '3:1048_YSV_137_CmdOpCo')
YSV138 = OnOffValve(client, plcName, '3:1048_YSV_138_PosOpen', '3:1048_YSV_138_PosClose', '3:1048_YSV_138_CmdOpCo')
YSV139 = OnOffValve(client, plcName, '3:1048_YSV_139_PosOpen', '3:1048_YSV_139_PosClose', '3:1048_YSV_139_CmdOpCo')
YSV101 = ControlValve(client, plcName, '3:1048_YSV_101_Position_Feedback', '3:1048_YSV_101_Position_Setpoint')
YSV103 = ControlValve(client, plcName, '3:1048_YSV_103_Position_Feedback', '3:1048_YSV_103_Position_Setpoint')
YSV100 = ControlValve(client, plcName, '3:1048_YSV_100_Position_Feedback', '3:1048_YSV_100_Position_Setpoint')
YSV102 = ControlValve(client, plcName, '3:1048_YSV_102_Position_Feedback', '3:1048_YSV_102_Position_Setpoint')
YSV104 = ControlValve(client, plcName, '3:1048_YSV_104_Position_Feedback', '3:1048_YSV_104_Position_Setpoint')
YSV105 = ControlValve(client, plcName, '3:1048_YSV_105_Position_Feedback', '3:1048_YSV_105_Position_Setpoint')
YSV112 = ControlValve(client, plcName, '3:1048_YSV_112_Position_Feedback', '3:1048_YSV_112_Position_Setpoint')
YSV113 = ControlValve(client, plcName, '3:1048_YSV_113_Position_Feedback', '3:1048_YSV_113_Position_Setpoint')
YSV114 = ControlValve(client, plcName, '3:1048_YSV_114_Position_Feedback', '3:1048_YSV_114_Position_Setpoint')
YSV118 = ControlValve(client, plcName, '3:1048_YSV_118_Position_Feedback', '3:1048_YSV_118_Position_Setpoint')
YSV123 = ControlValve(client, plcName, '3:1048_YSV_123_Position_Feedback', '3:1048_YSV_123_Position_Setpoint')
YSV131 = ControlValve(client, plcName, '3:1048_YSV_131_Position_Feedback', '3:1048_YSV_131_Position_Setpoint')
P014 = DiscretePump(client, plcName, '3:1048_P_014_CmdRun', '3:1048_P_014_StaRun')

# Initializaton
level_106 = 50
level_108 = 50
level_110 = 50
pressure_001 = 50

YSV002_status = "Close"
YSV005_status = "Close"
YSV008_status = "Close"
YSV113_status = "Close"
YSV114_status = "Close"
YSV130_status = "Close"
YSV132_status = "Close"
YSV134_status = "Close"
YSV137_status = "Close"
YSV138_status = "Close"
YSV139_status = "Close"
YSV101_status = "Close"
YSV103_status = "Close"
YSV100_status = "Close"
YSV102_status = "Close"
YSV104_status = "Close"
YSV105_status = "Close"
YSV112_status = "Close"
YSV118_status = "Close"
YSV123_status = "Close"
YSV131_status = "Close"
P014_status = False
diwSupplyOpened = False

# SIMulation main cycle
while (KeyboardInterrupt):
    # Main

    # DIW Supply status
    if YSV123_status == "Open":
        diwSupplyOpened = True
    else:
        diwSupplyOpened = False

    # --------------- B013 simulation ---------------
    # ----- Level increase -----
    if (YSV103_status == "Open" or YSV102_status == "Open" or (diwSupplyOpened and YSV137_status == "Open")):
        level_106 += 0.5
    
    # -----Level decrease -----
    if (YSV002_status == "Open" and P014_status and (YSV118_status == "Open" or YSV112_status == "Open")):
        level_106 -= 0.5
        pressure_001 = 2.1
    else:
        pressure_001 = 0
    # -----------------------------------------------

    # --------------- B015 simulation ---------------
    # ----- Level increase -----
    if (YSV105_status == "Open" or YSV100_status == "Open" or (diwSupplyOpened and YSV138_status == "Open")):
        level_110 += 0.5

    # -----Level decrease -----
    if (YSV002_status == "Open" and P014_status and (YSV118_status == "Open" or YSV114_status == "Open")):
        level_110 -= 0.5
        pressure_001 = 2.1
    else:
        pressure_001 = 0
    # -----------------------------------------------

    # ------------- B014 A/B simulation -------------
    # ----- Level increase -----
    if (YSV104_status == "Open" or YSV008_status == "Open" or (diwSupplyOpened and YSV139_status == "Open")):
        level_108 += 0.5

    # -----Level decrease -----
    if (YSV005_status == "Open" and P014_status and (YSV118_status == "Open" or YSV113_status == "Open")):
        level_108 -= 0.5
        pressure_001 = 2.1
    else:
        pressure_001 = 0
    # -----------------------------------------------

    # ---------------- Level limiter ----------------
    if level_106 < 0:
        level_106 = 0
    if level_106 > 100:
        level_106 = 100

    if level_108 < 0:
        level_108 = 0
    if level_108 > 100:
        level_108 = 100

    if level_110 < 0:
        level_110 = 0
    if level_110 > 100:
        level_110 = 100
    # -----------------------------------------------

    LIA106(client, plcName, '3:1048_PDIT_106_TReading', scale(level_106))
    LIA108(client, plcName, '3:1048_PDIT_108_TReading', scale(level_108))
    LIA110(client, plcName, '3:1048_PDIT_110_TReading', scale(level_110))
    PIT001(client, plcName, '3:1048_PIT_001_TReading', scale(pressure_001))
    YSV002_status = YSV002.SIM()
    YSV005_status = YSV005.SIM()
    YSV008_status = YSV008.SIM()
    YSV130_status = YSV130.SIM()
    YSV132_status = YSV132.SIM()
    YSV134_status = YSV134.SIM()
    YSV137_status = YSV137.SIM()
    YSV138_status = YSV138.SIM()
    YSV139_status = YSV139.SIM()

    if YSV101.SIM() >= valveOpenVal:
        YSV101_status = "Open"
    else:
        YSV101_status = "Close"

    if YSV103.SIM() >= valveOpenVal:
        YSV103_status = "Open"
    else:
        YSV103_status = "Close"

    if YSV100.SIM() >= valveOpenVal:
        YSV100_status = "Open"
    else:
        YSV100_status = "Close"

    if YSV102.SIM() >= valveOpenVal:
        YSV102_status = "Open"
    else:
        YSV102_status = "Close"

    if YSV104.SIM() >= valveOpenVal:
        YSV104_status = "Open"
    else:
        YSV104_status = "Close"

    if YSV105.SIM() >= valveOpenVal:
        YSV105_status = "Open"
    else:
        YSV105_status = "Close"

    if YSV112.SIM() >= valveOpenVal:
        YSV112_status = "Open"
    else:
        YSV112_status = "Close"

    if YSV113.SIM() >= valveOpenVal:
        YSV113_status = "Open"
    else:
        YSV113_status = "Close"

    if YSV114.SIM() >= valveOpenVal:
        YSV114_status = "Open"
    else:
        YSV114_status = "Close"

    if YSV118.SIM() >= valveOpenVal:
        YSV118_status = "Open"
    else:
        YSV118_status = "Close"

    if YSV123.SIM() >= valveOpenVal:
        YSV123_status = "Open"
    else:
        YSV123_status = "Close"

    if YSV131.SIM() >= valveOpenVal:
        YSV131_status = "Open"
    else:
        YSV131_status = "Close"

    P014_status = P014.SIM()

    print('Simulation Running!')

client.disconnect()
print('SIM aborted!')
