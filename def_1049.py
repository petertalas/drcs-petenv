from DeviceTypes import OnOffValve, ControlValve, DiscretePump, AnalogDevice, LevelSwitch
from myLib import scale
import time

from opc_client import OPCClient

exitFlag = 0


def simulator1049(threadName, client, plcName, timeout):
    # Devices
    FCV109 = ControlValve(
        client, plcName, '3:1049_HIC_109_PosFB', '3:1049_HIC_109_CmdPos')
    FCV129 = ControlValve(
        client, plcName, '3:1049_HIC_129_PosFB', '3:1049_HIC_129_CmdPos')
    YSV103 = OnOffValve(client, plcName, '3:1049_YSV_103_PosOpen',
                        '3:1049_YSV_103_PosClose', '3:1049_YSV_103_CmdOpCo')
    YSV104 = OnOffValve(client, plcName, '3:1049_YSV_104_PosOpen',
                        '3:1049_YSV_104_PosClose', '3:1049_YSV_104_CmdOpCo')
    YSV105 = OnOffValve(client, plcName, '3:1049_YSV_105_PosOpen',
                        '3:1049_YSV_105_PosClose', '3:1049_YSV_105_CmdOpCo')
    YSV142 = OnOffValve(client, plcName, '3:1049_YSV_142_PosOpen',
                        '3:1049_YSV_142_PosClose', '3:1049_YSV_142_CmdOpCo')
    YSV143 = OnOffValve(client, plcName, '3:1049_YSV_143_PosOpen',
                        '3:1049_YSV_143_PosClose', '3:1049_YSV_143_CmdOpCo')
    P017 = DiscretePump(
        client, plcName, '3:1049_P_017_CmdRun', '3:1049_P_017_StaRun')
    P018 = DiscretePump(
        client, plcName, '3:1049_P_018_CmdRun', '3:1049_P_018_StaRun')
    P020 = DiscretePump(
        client, plcName, '3:1049_P_020_CmdRun', '3:1049_P_020_StaRun')
    P021 = DiscretePump(
        client, plcName, '3:1049_P_021_CmdRun', '3:1049_P_021_StaRun')
    PDIT122 = AnalogDevice
    PIT110 = AnalogDevice
    PIT111 = AnalogDevice
    PIT121 = AnalogDevice
    PIT123 = AnalogDevice
    LS134 = LevelSwitch
    LS150 = LevelSwitch
    LS152 = LevelSwitch

    # 1047 transfer line
    FCV120_1047 = client.get_root_node().get_child(
        ['0:Objects', plcName, '3:Inputs', '3:1047_HIC_120_PosFB'])
    FCV180_1047 = client.get_root_node().get_child(
        ['0:Objects', plcName, '3:Inputs', '3:1047_HIC_180_PosFB'])
    P012_1047 = client.get_root_node().get_child(
        ['0:Objects', plcName, '3:Inputs', '3:1047_P_012_StaRun'])
    P013_1047 = client.get_root_node().get_child(
        ['0:Objects', plcName, '3:Inputs', '3:1047_P_013_StaRun'])

    # 1048 transfer line
    YSV118_1048 = client.get_root_node().get_child(
        ['0:Objects', plcName, '3:Inputs', '3:1048_YSV_118_Feedback'])
    P014_1048 = client.get_root_node().get_child(
        ['0:Objects', plcName, '3:Inputs', '3:1048_P_014_StaRun'])

    # Initializaton
    FCV109_status = 0
    FCV129_status = 0
    YSV103_status = "Close"
    YSV104_status = "Close"
    YSV105_status = "Close"
    YSV142_status = "Close"
    YSV143_status = "Close"
    P017_status = False
    P018_status = False
    P020_status = False
    P021_status = False
    level_122 = 50
    pressure_110 = 0
    pressure_111 = 0
    level_121 = 50
    level_123 = 50
    LS134_state = False
    LS150_state = False
    LS152_state = False
    FCV120_1047_status = 0
    FCV180_1047_status = 0
    P012_1047_status = False
    P013_1047_status = False
    YSV118_1048_status = 0
    P014_1048_status = False
    transferLine1047_1 = False
    transferLine1047_2 = False
    transferLine1048_1 = False

    # SIMulation main cycle
    while (time.time() < timeout):
        if exitFlag:
            threadName.exit()
        # Main
        # ------------- 1047 transfer active ------------
        if FCV120_1047_status == 'Open' and P013_1047_status:
            transferLine1047_1 = True
        else:
            transferLine1047_1 = False
        if FCV180_1047_status == 'Open' and P012_1047_status:
            transferLine1047_2 = True
        else:
            transferLine1047_2 = False
        # -----------------------------------------------
        # ------------- 1048 transfer active ------------
        if YSV118_1048_status == 'Open' and P014_1048_status:
            transferLine1048_1 = True
        else:
            transferLine1048_1 = False
        # -----------------------------------------------
        # --------------- B007 simulation ---------------
        # Level increase
        if transferLine1047_1 or transferLine1047_2 or YSV142_status == 'Open':
            level_121 += 0.0001
        # -----------------------------------------------
        # Level decrease
        if P020_status and FCV109_status == 'Open':
            level_121 -= 1
        # -----------------------------------------------
        # --------------- B008 simulation ---------------
        # Level increase
        if transferLine1048_1 or YSV143_status == 'Open':
            level_122 += 1
        # -----------------------------------------------
        # Level decrease
        if P021_status and YSV103_status == 'Open' and FCV129_status >= 25:
            level_122 -= 1
        # -----------------------------------------------
        # ------------ B009 / 010 simulation ------------
        # Level increase
        if P017_status or P018_status or YSV104_status == 'Open':
            level_123 += 1
        
        if P017_status and P018_status:
            level_123 += 2

        # -----------------------------------------------
        # Level decrease
        if P021_status and YSV105_status == 'Open' and FCV129_status >= 25:
            level_123 -= 1
        # -----------------------------------------------

        FCV120_1047_status = OPCClient.getValue(client, FCV120_1047)
        if FCV120_1047_status >= 25:
            FCV120_1047_status = 'Open'
        else:
            FCV120_1047_status = 'Close'
        FCV180_1047_status = OPCClient.getValue(client, FCV180_1047)
        if FCV180_1047_status >= 25:
            FCV180_1047_status = 'Open'
        else:
            FCV180_1047_status = 'Close'
        P012_1047_status = OPCClient.getValue(client, P012_1047)
        P013_1047_status = OPCClient.getValue(client, P013_1047)
        YSV118_1048_status = OPCClient.getValue(client, YSV118_1048)
        if YSV118_1048_status >= 25:
            YSV118_1048_status = 'Open'
        else:
            YSV118_1048_status = 'Close'
        P014_1048_status = OPCClient.getValue(client, P014_1048)
        FCV109_status = FCV109.SIM()
        FCV129_status = FCV129.SIM()
        YSV103_status = YSV103.SIM()
        YSV104_status = YSV104.SIM()
        YSV105_status = YSV105.SIM()
        YSV142_status = YSV142.SIM()
        YSV143_status = YSV143.SIM()
        P017_status = P017.SIM()
        P018_status = P018.SIM()
        P020_status = P020.SIM()
        P021_status = P021.SIM()
        PDIT122(client, plcName, '3:1049_PDIT_122_TReading', scale(level_122))
        PIT110(client, plcName, '3:1049_PIT_110_TReading', scale(pressure_110))
        PIT111(client, plcName, '3:1049_PIT_111_TReading', scale(pressure_111))
        PIT121(client, plcName, '3:1049_PIT_121_TReading', scale(level_121))
        PIT123(client, plcName, '3:1049_PIT_123_TReading', scale(level_123))
        LS134(client, plcName, '3:1049_LS_134_SWActiv', LS134_state)
        LS150(client, plcName, '3:1049_LS_150_SWActiv', LS150_state)
        LS152(client, plcName, '3:1049_LS_152_SWActiv', LS152_state)
        #print('Simulation Running!')
