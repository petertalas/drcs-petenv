# NOTE Siemens scaling algorithm
# OUT = [((FLOAT(IN) - K1)/(K2 - K1)) * (HI_LIM - LO_LIM)] + LO_LIM
# K1 = 27648
# K2 = -27648

from opc_client import OPCClient

import math

def scale(input, lo_lim=0, hi_lim=100):
    if lo_lim <= input <= hi_lim:
        return int((((input - lo_lim)/(hi_lim - lo_lim)) * (27648 - 0)) + 0)
    elif input < lo_lim:
        return 0
    elif input > hi_lim:
        return 27648

def biScale(input, lo_lim=-100, hi_lim=100):
    if lo_lim <= input <= hi_lim:
        return int((((input - lo_lim)/(hi_lim - lo_lim)) * (27648 - (-27648))) + (-27648))
    else:
        return "scaling error"

#client.get_root_node().get_child(['0:Objects', '3:RGTCS_PLC01', '3:DataBlocksInstance', '3:DEV_Tgt-RGT1040:Proc-Virt-100_iDB', '3:Inputs', '3:si_ActStepID'])
#client.get_root_node().get_child(['0:Objects', '3:RGTCS_PLC01', '3:DataBlocksInstance', '3:DEV_Tgt-RGT1040:Proc-Virt-100_iDB', '3:Inputs', '3:si_ActStepID'])

class readDBvalue():
    def __init__(self, client, plcName, dbPath, valuePath):
      self.client = client
      self.plcName = plcName
      self.dbPath = dbPath
      self.valuePath = valuePath
    
    def read(self):
        #dbValue = self.client.get_root_node().get_child(['0:Objects', self.plcName, '3:DataBlocksInstance', self.dbPath, '3:Inputs', self.valuePath])
        value = self.client.get_root_node().get_child(
            ['0:Objects', self.plcName, 'DataBlockInstance', self.dbPath, '3:Inputs', self.valuePath])

        valueFromDB = OPCClient.getValue(self.client, value)
        return valueFromDB
