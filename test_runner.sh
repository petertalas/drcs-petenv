#!/bin/sh
now=`date +%Y%m%d_%H%M%S`
pytest -v drcsAutoTest.py --full-trace --self-contained-html --html=./html/DRCSautotest_report_$now.html