# This is a placeholder for multithreaded simulator for the whole system.
# It is a test!
import threading
import time
from opc_client import OPCClient
import def_1047
import def_1048
import def_1049

print("Running time in minutes: ")
input = input()

endTime = float(input)  # minutes
timeout = time.time() + 60*endTime

ip = "192.168.0.223"
plcName = '3:DRCS_PLC001'
client = OPCClient(ip, timeout=600)
client.connect()


class sim1047 (threading.Thread):
    def __init__(self, threadID, name, client, plcName, timeout):
        threading.Thread.__init__(self)
        self.threadID = threadID
        self.name = name
        self.client = client
        self.plcName = plcName
        self.timeout = timeout

    def run(self):
        print("Start " + self.name)
        def_1047.simulator1047(self.name, self.client,
                               self.plcName, self.timeout)
        print("Exiting " + self.name)


class sim1048(threading.Thread):
    def __init__(self, threadID, name, client, plcName, timeout):
        threading.Thread.__init__(self)
        self.threadID = threadID
        self.name = name
        self.client = client
        self.plcName = plcName
        self.timeout = timeout

    def run(self):
        print("Start " + self.name)
        def_1048.simulator1048(self.name, self.client,
                               self.plcName, self.timeout)
        print("Exiting " + self.name)


class sim1049(threading.Thread):
    def __init__(self, threadID, name, client, plcName, timeout):
        threading.Thread.__init__(self)
        self.threadID = threadID
        self.name = name
        self.client = client
        self.plcName = plcName
        self.timeout = timeout

    def run(self):
        print("Start " + self.name)
        def_1049.simulator1049(self.name, self.client,
                               self.plcName, self.timeout)
        print("Exiting " + self.name)


thread_1 = sim1047(1, '1047_Simulator', client, plcName, timeout)
thread_2 = sim1048(2, '1048_Simulator', client, plcName, timeout)
thread_3 = sim1049(3, '1049_Simulator', client, plcName, timeout)

thread_1.start()
thread_2.start()
thread_3.start()
thread_1.join()
thread_2.join()
thread_3.join()

client.disconnect()
print("Exit from main thread!")
