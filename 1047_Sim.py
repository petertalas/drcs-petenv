from DeviceTypes import *
from opc_client import OPCClient
from myLib import scale

# Constants
ip = "192.168.0.223"
plcName = '3:DRCS_PLC001'

# Connect to PLC
client = OPCClient(ip, timeout=600)
client.connect()

# Devices
LIA101_1047 = AnalogDevice
LIA111_1047 = AnalogDevice
LIA121_1047 = AnalogDevice
PIT115_1047 = AnalogDevice
PIT125_1047 = AnalogDevice
YSV200 = OnOffValve(client, plcName, '3:1047_YSV_200_PosOpen', '3:1047_YSV_200_PosClose', '3:1047_YSV_200_CmdOpCo')
YSV110 = OnOffValve(client, plcName, '3:1047_YSV_110_PosOpen', '3:1047_YSV_110_PosClose', '3:1047_YSV_110_CmdOpCo')
YSV210 = OnOffValve(client, plcName, '3:1047_YSV_210_PosOpen', '3:1047_YSV_210_PosClose', '3:1047_YSV_210_CmdOpCo')
YSV100 = OnOffValve(client, plcName, '3:1047_YSV_100_PosOpen', '3:1047_YSV_100_PosClose', '3:1047_YSV_100_CmdOpCo')
YSV220 = OnOffValve(client, plcName, '3:1047_YSV_220_PosOpen', '3:1047_YSV_220_PosClose', '3:1047_YSV_220_CmdOpCo')
YSV230 = OnOffValve(client, plcName, '3:1047_YSV_230_PosOpen', '3:1047_YSV_230_PosClose', '3:1047_YSV_230_CmdOpCo')
FCV160 = ControlValve(client, plcName, '3:1047_HIC_160_PosFB', '3:1047_HIC_160_CmdPos')
FCV170 = ControlValve(client, plcName, '3:1047_HIC_170_PosFB', '3:1047_HIC_170_CmdPos')
FCV240 = ControlValve(client, plcName, '3:1047_HIC_240_PosFB', '3:1047_HIC_240_CmdPos')
FCV120 = ControlValve(client, plcName, '3:1047_HIC_120_PosFB', '3:1047_HIC_120_CmdPos')
FCV180 = ControlValve(client, plcName, '3:1047_HIC_180_PosFB', '3:1047_HIC_180_CmdPos')
P012 = DiscretePump(client, plcName, '3:1047_P_012_CmdRun', '3:1047_P_012_StaRun')
P013 = DiscretePump(client, plcName, '3:1047_P_013_CmdRun', '3:1047_P_013_StaRun')
LS122 = LevelSwitch

# Initializaton
level_101 = 85.00000
level_111 = 85.00000
level_121 = 85.00000
press_115 = 0.00000
press_125 = 0.00000

# SIMulation main cycle
while (KeyboardInterrupt):
    # Main

# Draining
    if YSV200.SIM() == "Open":
        level_101 += 0.5
    
    if YSV210.SIM() == "Open":
        level_111 += 0.5

    if (YSV220.SIM() == "Open") and (YSV230.SIM() == "Open"):
        level_121 += 0.5
    if level_121 >= 96:
        LS122(client, plcName, '3:1047_LS_122_SWActiv', True)
    else:
        LS122(client, plcName, '3:1047_LS_122_SWActiv', False)

# pumping
    if P012.SIM() and (YSV110.SIM() == "Open") and ((FCV160.SIM() == scale(80)) or (FCV180.SIM() == scale(80))) :
        level_101 -= 0.5
        press_115 = 2.1
    else:
        press_115 = 0

    if P012.SIM() and (YSV100.SIM() == "Open") and ((FCV170.SIM() == scale(80)) or (FCV180.SIM() == scale(80))) :
        level_111 -= 0.5
        press_115 = 2.2
    else:
        press_115 = 0

    if P013.SIM() and ((FCV240.SIM() == scale(80)) or (FCV120.SIM() == scale(80))):
        level_121 -= 0.5
        press_125 = 3.2
    else:
        press_125 = 0

    if level_101 < 0:
        level_101 = 0
    if level_111 < 0:
        level_111 = 0
    if level_121 < 0:
        level_121 = 0

    if level_101 > 100:
        level_101 = 100
    if level_111 > 100:
        level_111 = 100
    if level_121 > 100:
        level_121 = 100

    # Values to devices:
    LIA101_1047(client, plcName, '3:1047_PIT_101_TReading', scale(level_101))
    LIA111_1047(client, plcName, '3:1047_PIT_111_TReading', scale(level_111))
    LIA121_1047(client, plcName, '3:1047_PDIT_121_TReading', scale(level_121))
    PIT115_1047(client, plcName, '3:1047_PIT_115_TReading', scale(press_115))
    PIT125_1047(client, plcName, '3:1047_PIT_125_TReading', scale(press_125))

    YSV200.SIM()
    YSV110.SIM()
    YSV210.SIM()
    YSV100.SIM()
    YSV220.SIM()
    YSV230.SIM()
    FCV160.SIM()
    FCV170.SIM()
    FCV240.SIM()
    FCV120.SIM()
    FCV180.SIM()

    P012.SIM()
    P013.SIM()

    print('Simulation Running!')

client.disconnect()
print('SIM aborted!')
