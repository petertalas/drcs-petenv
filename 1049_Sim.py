from DeviceTypes import OnOffValve, ControlValve, DiscretePump, AnalogDevice, LevelSwitch
from opc_client import OPCClient
from myLib import scale

# Constants
ip = "192.168.0.223"
plcName = '3:DRCS_PLC001'

valveOpenVal = 80
valveCloseVal = 10

# Connect to PLC
client = OPCClient(ip, timeout=600)
client.connect()

# Devices
FCV109 = ControlValve(client, plcName, '3:1049_HIC_109_PosFB', '3:1049_HIC_109_CmdPos')
FCV129 = ControlValve(client, plcName, '3:1049_HIC_129_PosFB', '3:1049_HIC_129_CmdPos')
YSV103 = OnOffValve(client, plcName, '3:1049_YSV_103_PosOpen', '3:1049_YSV_103_PosClose', '3:1049_YSV_103_CmdOpCo')
YSV104 = OnOffValve(client, plcName, '3:1049_YSV_104_PosOpen', '3:1049_YSV_104_PosClose', '3:1049_YSV_104_CmdOpCo')
YSV105 = OnOffValve(client, plcName, '3:1049_YSV_105_PosOpen', '3:1049_YSV_105_PosClose', '3:1049_YSV_105_CmdOpCo')
YSV142 = OnOffValve(client, plcName, '3:1049_YSV_142_PosOpen', '3:1049_YSV_142_PosClose', '3:1049_YSV_142_CmdOpCo')
YSV143 = OnOffValve(client, plcName, '3:1049_YSV_143_PosOpen', '3:1049_YSV_143_PosClose', '3:1049_YSV_143_CmdOpCo')
P017 = DiscretePump(client, plcName, '3:1049_P_017_CmdRun', '3:1049_P_017_StaRun')
P018 = DiscretePump(client, plcName, '3:1049_P_018_CmdRun', '3:1049_P_018_StaRun')
P020 = DiscretePump(client, plcName, '3:1049_P_020_CmdRun', '3:1049_P_020_StaRun')
P021 = DiscretePump(client, plcName, '3:1049_P_021_CmdRun', '3:1049_P_021_StaRun')
PDIT122 = AnalogDevice
PIT110 = AnalogDevice
PIT111 = AnalogDevice
PIT121 = AnalogDevice
PIT123 = AnalogDevice
LS134 = LevelSwitch
LS150 = LevelSwitch
LS152 = LevelSwitch

# Initializaton
FCV109_status = 0
FCV129_status = 0
YSV103_status = "Close"
YSV104_status = "Close"
YSV105_status = "Close"
YSV142_status = "Close"
YSV143_status = "Close"
P017_status = False
P018_status = False
P020_status = False
P021_status = False
level_122 = 50
level_110 = 50
level_111 = 50
level_121 = 50
level_123 = 50
LS134_state = False
LS150_state = False
LS152_state = False

# SIMulation main cycle
while (KeyboardInterrupt):
    # Main

    # --------------- B013 simulation ---------------

    # -----------------------------------------------

    FCV109_status = FCV109.SIM()
    FCV129_status = FCV129.SIM()
    YSV103_status = YSV103.SIM()
    YSV104_status = YSV104.SIM()
    YSV105_status = YSV105.SIM()
    YSV142_status = YSV142.SIM()
    YSV143_status = YSV143.SIM()
    P017_status = P017.SIM()
    P018_status = P018.SIM()
    P020_status = P020.SIM()
    P021_status = P021.SIM()
    PDIT122(client, plcName, '3:1049_PDIT_122_TReading', scale(level_122))
    PIT110(client, plcName, '3:1049_PIT_110_TReading', scale(level_110))
    PIT111(client, plcName, '3:1049_PIT_111_TReading', scale(level_111))
    PIT121(client, plcName, '3:1049_PIT_121_TReading', scale(level_121))
    PIT123(client, plcName, '3:1049_PIT_123_TReading', scale(level_123))
    LS134(client, plcName, '3:1049_LS_134_SWActiv', LS134_state)
    LS150(client, plcName, '3:1049_LS_150_SWActiv', LS150_state)
    LS152(client, plcName, '3:1049_LS_152_SWActiv', LS152_state)

    print('Simulation Running!')

client.disconnect()
print('SIM aborted!')
