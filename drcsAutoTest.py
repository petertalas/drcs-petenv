from time import sleep
from epics.ca import clear_cache
import pytest_check as check
from epics import caget, caput
from myLib import scale
from opc_client import OPCClient
from deviceClasses import *

# Get devices from a list /csv/
sleepval = 0.5
myFile = open("myData.csv", "r")

myPT = [" "]
myCV = [" "]
myYSV = [" "]
myP = [" "]
myLBCA = [" "]

for aline in myFile:
    vals = aline.split(";")
    if vals[1] == "INI":
        myTestInit = Init(vals[2], vals[3], vals[4])
    if vals[1] == ("PT" or "PDT"):
        myPT.append(Analog(vals[2], vals[3], vals[4], float(vals[5]), float(vals[6]), float(vals[7]), float(
            vals[8]), float(vals[9]), float(vals[10]), float(vals[11]), float(vals[12]), float(vals[13]), float(vals[14])))
    if vals[1] == "CV":
        myCV.append(CV(vals[2], vals[3], vals[4], vals[5], vals[6]))
    if vals[1] == "YSV":
        myYSV.append(ON_OFF_VALVE(
            vals[2], vals[3], vals[4], vals[5], vals[6], vals[7]))
    if vals[1] == "PUMP":
        myP.append(PUMP(vals[2], vals[3], vals[4], vals[5]))
    if vals[1] == "LBCA":
        myLBCA.append(LBCA(vals[2], vals[3], vals[4]))

myFile.close()
# Opc client
ioc_ip = myTestInit.IocIp
plc_ip = myTestInit.PlcIp
client = OPCClient(plc_ip)


def test_AnalogMeasurements():
    i = 1
    while i < len(myPT):
        print(myPT[i].Tag)
        sclerrormsg = "@ " + myPT[i].Tag + \
            " Wrong scaling configuration in the PLC!"
        alarmerrormsg = "@ " + myPT[i].Tag + \
            " Alarm triggered but not at the correct Value!"
        # init
        caput(myPT[i].PvName+":P_Limit_HIHI", myPT[i].ValHiHi)
        caput(myPT[i].PvName+":P_Limit_HI", myPT[i].ValHi)
        caput(myPT[i].PvName+":P_Limit_LO", myPT[i].ValLo)
        caput(myPT[i].PvName+":P_Limit_LOLO", myPT[i].ValLoLo)
        client.connect()
        # ---- Get nodes ----
        measValNode = client.get_root_node().get_child(
            ['0:Objects', '3:'+myTestInit.OpcName, '3:Inputs', '3:'+myPT[i].OpcInNode])
        sleep(sleepval)
        # ------------------- Checks -------------------
        # Scale check
        OPCClient.setValue(client, measValNode, scale(
            myPT[i].ChkVal1, myPT[i].RngLo, myPT[i].RngHi))
        sleep(sleepval)
        check.equal(myPT[i].ChkVal1, round(caget(myPT[
            i].PvName+":MeasValue")), sclerrormsg)
        OPCClient.setValue(client, measValNode, scale(
            myPT[i].ChkVal2, myPT[i].RngLo, myPT[i].RngHi))
        sleep(sleepval)
        check.equal(myPT[i].ChkVal2, round(caget(myPT[
            i].PvName+":MeasValue")), sclerrormsg)
        OPCClient.setValue(client, measValNode, scale(
            myPT[i].ChkVal3, myPT[i].RngLo, myPT[i].RngHi))
        sleep(sleepval)
        check.equal(myPT[i].ChkVal3, round(caget(myPT[
            i].PvName+":MeasValue")), sclerrormsg)
        OPCClient.setValue(client, measValNode, scale(
            myPT[i].ChkVal4, myPT[i].RngLo, myPT[i].RngHi))
        sleep(sleepval)
        check.equal(myPT[i].ChkVal4, round(caget(myPT[
            i].PvName+":MeasValue")), sclerrormsg)
        # Ack alarms
        OPCClient.setValue(client, measValNode, scale(
            myPT[i].RngHi / 2, myPT[i].RngLo, myPT[i].RngHi))
        sleep(sleepval)
        caput(myPT[i].PvName+":Cmd_AckAlarm", True)
        sleep(0.01)
        caput(myPT[i].PvName+":Cmd_AckAlarm", False)
        # Alarm checks
        # Low alarms
        simVal = myPT[i].RngHi / 2
        while simVal > myPT[i].RngLo:
            OPCClient.setValue(client, measValNode, scale(
                simVal, myPT[i].RngLo, myPT[i].RngHi))
            sleep(sleepval)
            if caget(myPT[i].PvName+":LO") == 1:
                check.less_equal(caget(myPT[i].PvName+":MeasValue"), myPT[
                    i].ValLo, alarmerrormsg)
            if caget(myPT[i].PvName+":LOLO") == 1:
                check.less_equal(caget(myPT[i].PvName+":MeasValue"), myPT[
                    i].ValLoLo, alarmerrormsg)
            simVal -= myPT[i].RngHi*0.1
        # Ack alarms
        OPCClient.setValue(client, measValNode, scale(
            myPT[i].RngHi / 2, myPT[i].RngLo, myPT[i].RngHi))
        sleep(sleepval)
        caput(myPT[i].PvName+":Cmd_AckAlarm", True)
        sleep(0.01)
        caput(myPT[i].PvName+":Cmd_AckAlarm", False)
        # High alarms
        simVal = myPT[i].RngHi / 2
        while simVal < myPT[i].RngHi:
            OPCClient.setValue(client, measValNode, scale(
                simVal, myPT[i].RngLo, myPT[i].RngHi))
            sleep(sleepval)
            if caget(myPT[i].PvName+":HI") == 1:
                check.greater_equal(caget(myPT[i].PvName+":MeasValue"), myPT[
                    i].ValHi, alarmerrormsg)
            if caget(myPT[i].PvName+":HIHI") == 1:
                check.greater_equal(caget(myPT[i].PvName+":MeasValue"), myPT[
                    i].ValHiHi, alarmerrormsg)
            simVal += myPT[i].RngHi*0.1
            sleep(sleepval)
        client.disconnect()
        sleep(sleepval)
        i += 1


def test_LBCA():
    i = 1
    while i < len(myLBCA):
        print(myLBCA[i].Tag)
        errormsg1 = "@ " + \
            myLBCA[i].Tag + \
            " Close not setted in the IOC! Switch Closed State indication NOT working!"
        errormsg2 = "@ " + \
            myLBCA[i].Tag + \
            " Open not setted in the IOC! Switch Open State indication NOT working!"
        errormsg3 = "@ " + myLBCA[i].Tag + " Warning not setted in the IOC!"
        errormsg4 = "@ " + myLBCA[i].Tag + " Alarm not setted in the IOC!"
        client.connect()
        plcInput = client.get_root_node().get_child(
            ['0:Objects', '3:'+myTestInit.OpcName, '3:Inputs', '3:'+myLBCA[i].PlcIn])
        # Test LS closed state
        OPCClient.setValue(client, plcInput, True)
        sleep(sleepval)
        check.is_true(caget(myLBCA[i].PvName+":Closed")==1, errormsg1)
        # Test Warning active
        check.is_true(caget(myLBCA[i].PvName+":Warning")==1, errormsg3)
        # Test Alarm active
        check.is_true(caget(myLBCA[i].PvName+":Alarm")==1, errormsg4)
        # Test LS opened state
        sleep(sleepval)
        OPCClient.setValue(client, plcInput, False)
        sleep(sleepval)
        check.is_true(caget(myLBCA[i].PvName+":Opened") == 1, errormsg2)
        client.disconnect()
        i += 1


def test_Pumps():
    i = 1
    while i != len(myP):
        print(myP[i].Tag)
        errormsg1 = "@ " + myP[i].Tag + " Cant't control the pump!"
        errormsg2 = "@ " + myP[i].Tag + " Cant't start the pump!"
        errormsg3 = "@ " + myP[i].Tag + " Cant't stop the pump!"
        # Connect to the PLC
        client.connect()
        sleep(sleepval)
        P_Command = client.get_root_node().get_child(
            ['0:Objects', '3:'+myTestInit.OpcName, '3:Outputs', '3:'+myP[i].Control])
        P_Status = client.get_root_node().get_child(
            ['0:Objects', '3:'+myTestInit.OpcName, '3:Inputs', '3:'+myP[i].Status])
        # Check Pump running
        caput(myP[i].PvName+":Cmd_ManuStart", True)
        sleep(sleepval)
        retVal = OPCClient.getValue(client, P_Command)
        sleep(sleepval)
        OPCClient.setValue(client, P_Status, retVal)
        sleep(sleepval)
        check.is_true(OPCClient.getValue(client, P_Command) == 1, errormsg1)
        check.is_true(caget(myP[i].PvName+":Running") == 1, errormsg2)
        check.is_true(caget(myP[i].PvName+":Stopped") == 0, errormsg2)
        sleep(sleepval)
        # Check Pump stoped
        caput(myP[i].PvName+":Cmd_ManuStop", True)
        sleep(sleepval)
        retVal = OPCClient.getValue(client, P_Command)
        sleep(sleepval)
        OPCClient.setValue(client, P_Status, retVal)
        sleep(sleepval)
        check.is_true(OPCClient.getValue(client, P_Command) == 0, errormsg1)
        check.is_true(caget(myP[i].PvName+":Stopped") == 1, errormsg3)
        check.is_true(caget(myP[i].PvName+":Running") == 0, errormsg3)
        # OPC client disconnect
        client.disconnect()
        sleep(sleepval)
        i += 1


def test_ControlValves():
    i = 1
    while i < len(myCV):
        print(myCV[i].Tag)
        errormsg = "@ " + myCV[i].Tag + " Cant't set the specified value!"
        errormsg1 = "@ " + myCV[i].Tag + " Power cut is not indicated!"
        # Connect to the PLC
        client.connect()
        CvSp = client.get_root_node().get_child(
            ['0:Objects', '3:'+myTestInit.OpcName, '3:Outputs', '3:'+myCV[i].SetPoint])
        CvFb = client.get_root_node().get_child(
            ['0:Objects', '3:'+myTestInit.OpcName, '3:Inputs', '3:'+myCV[i].FeedBack])
        RdyStatus = client.get_root_node().get_child(
            ['0:Objects', '3:'+myTestInit.OpcName, '3:Inputs', '3:'+myCV[i].StaPnR])
        # Set power
        OPCClient.setValue(client, RdyStatus, True)
        # Ack alarms
        #caput(myCV[i].PvName+":AckAlarm", True)
        # Check SPs and FBs and zeroing
        caput(myCV[i].PvName+":P_Setpoint", 0)
        # Return Setpoint to the Feedback
        retVal = OPCClient.getValue(client, CvSp)
        sleep(sleepval)
        OPCClient.setValue(client, CvFb, retVal)
        sleep(sleepval)
        check.is_true(OPCClient.getValue(client, CvSp) == scale(0), errormsg)
        check.is_true(caget(myCV[i].PvName+":FB_Manipulated") == 0, errormsg)
        sleep(sleepval)
        # Set first value (25%)
        caput(myCV[i].PvName+":P_Setpoint", 25)
        sleep(sleepval)
        # Return Setpoint to the Feedback
        retVal = OPCClient.getValue(client, CvSp)
        sleep(sleepval)
        OPCClient.setValue(client, CvFb, retVal)
        sleep(sleepval)
        # Check value
        check.is_true(OPCClient.getValue(client, CvSp) == scale(25), errormsg)
        check.is_true(caget(myCV[i].PvName+":FB_Manipulated") == 25, errormsg)
        sleep(sleepval)
        # Set secound value (50%)
        caput(myCV[i].PvName+":P_Setpoint", 50)
        sleep(sleepval)
        # Return Setpoint to the Feedback
        retVal = OPCClient.getValue(client, CvSp)
        sleep(sleepval)
        OPCClient.setValue(client, CvFb, retVal)
        sleep(sleepval)
        # Check value
        check.is_true(OPCClient.getValue(client, CvSp) == scale(50), errormsg)
        check.is_true(caget(myCV[i].PvName+":FB_Manipulated") == 50, errormsg)
        sleep(sleepval)
        # Set secound value (75%)
        caput(myCV[i].PvName+":P_Setpoint", 75)
        sleep(sleepval)
        # Return Setpoint to the Feedback
        retVal = OPCClient.getValue(client, CvSp)
        sleep(sleepval)
        OPCClient.setValue(client, CvFb, retVal)
        sleep(sleepval)
        # Check value
        check.is_true(OPCClient.getValue(client, CvSp) == scale(75), errormsg)
        check.is_true(caget(myCV[i].PvName+":FB_Manipulated") == 75, errormsg)
        sleep(sleepval)
        # Set secound value (100%)
        caput(myCV[i].PvName+":P_Setpoint", 100)
        sleep(sleepval)
        # Return Setpoint to the Feedback
        retVal = OPCClient.getValue(client, CvSp)
        sleep(sleepval)
        OPCClient.setValue(client, CvFb, retVal)
        sleep(sleepval)
        # Check value
        check.is_true(OPCClient.getValue(client, CvSp) == scale(100), errormsg)
        check.is_true(caget(myCV[i].PvName+":FB_Manipulated") == 100, errormsg)
        # OPC client disconnect
        client.disconnect()
        i += 1


def test_OnOffValves():
    i = 1
    while i < len(myYSV):
        print(myYSV[i].Tag)
        errormsg1 = "@ " + myYSV[i].Tag + " Cant't control the valve!"
        errormsg2 = "@ " + myYSV[i].Tag + " Valve did not open!"
        errormsg3 = "@ " + myYSV[i].Tag + " Valve did not close!"
        errormsg4 = "@ " + myYSV[i].Tag + " Both feedback signal is active!"
        # Connect to the PLC
        client.connect()
        YsvCommand = client.get_root_node().get_child(
            ['0:Objects', '3:'+myTestInit.OpcName, '3:Outputs', '3:'+myYSV[i].Command])
        YsvFbOpen = client.get_root_node().get_child(
            ['0:Objects', '3:'+myTestInit.OpcName, '3:Inputs',  '3:'+myYSV[i].FeedBack_open])
        YsvFbClose = client.get_root_node().get_child(
            ['0:Objects', '3:'+myTestInit.OpcName, '3:Inputs',  '3:'+myYSV[i].FeedBack_close])
        # Check Open state
        caput(myYSV[i].PvName+":Cmd_ManuOpen", True)
        sleep(sleepval)
        retVal = OPCClient.getValue(client, YsvCommand)
        sleep(sleepval)
        if retVal == True:
            OPCClient.setValue(client, YsvFbOpen, retVal)
            OPCClient.setValue(client, YsvFbClose, not(retVal))
        else:
            OPCClient.setValue(client, YsvFbOpen, retVal)
            OPCClient.setValue(client, YsvFbClose, not(retVal))
        sleep(sleepval)
        check.is_true(OPCClient.getValue(client, YsvCommand) == 1, errormsg1)
        check.is_true(caget(myYSV[i].PvName+":Opened") == 1, errormsg2)
        sleep(sleepval)
        check.is_false(caget(myYSV[i].PvName+":Closed") ==
                       (caget(myYSV[i].PvName+":Opened"), errormsg4))
        sleep(sleepval)
        # Check Close state
        caput(myYSV[i].PvName+":Cmd_ManuClose", True)
        sleep(sleepval)
        retVal = OPCClient.getValue(client, YsvCommand)
        sleep(sleepval)
        if retVal == True:
            OPCClient.setValue(client, YsvFbOpen, retVal)
            OPCClient.setValue(client, YsvFbClose, not(retVal))
        else:
            OPCClient.setValue(client, YsvFbOpen, retVal)
            OPCClient.setValue(client, YsvFbClose, not(retVal))
        sleep(sleepval)
        check.is_true(OPCClient.getValue(client, YsvCommand) == 0, errormsg1)
        check.is_true(caget(myYSV[i].PvName+":Closed") == 1, errormsg3)
        sleep(sleepval)
        check.is_false(caget(myYSV[i].PvName+":Closed") ==
                       (caget(myYSV[i].PvName+":Opened"), errormsg4))
        # OPC client disconnect
        client.disconnect()
        i += 1
